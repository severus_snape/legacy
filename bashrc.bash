# Automatically added by the Guix install script.
if [ -n "$GUIX_ENVIRONMENT" ]; then
    if [[ $PS1 =~ (.*)"\\$" ]]; then
        PS1="${BASH_REMATCH[1]} [env]\\\$ "
    fi
fi

# dirtrack https://emacs.stackexchange.com/questions/5589/
if [ "$INSIDE_EMACS" ]; then
    export PS1="[\h:\w] $ "
fi
