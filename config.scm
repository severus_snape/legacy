(use-modules (dotfiles private)
             (gnu home services guix)
             (gnu home services shells)
             (gnu home services)
             (gnu packages audio)
             (gnu packages)
             (gnu services)
             (guix channels)
             (guix ci)
             (guix derivations)
             (guix gexp)
             (guix packages)
             (guix store))

(define my-packages
  (list
   "audacious"
   "autoconf"                           ;man page
   "automake"                           ;man page
   "bind:utils"                         ;dig
   "compiledb"
   "curl"
   "emacs-alect-themes"
   "emacs-amx"
   "emacs-auctex"
   "emacs-avy"
   "emacs-clang-format"
   "emacs-cmake-mode"
   "emacs-company"
   "emacs-counsel"
   "emacs-debbugs"
   "emacs-default-text-scale"
   "emacs-direnv"
   "emacs-dtrt-indent"
   "emacs-dts-mode"
   "emacs-evil"
   "emacs-evil-numbers"
   "emacs-evil-surround"
   "emacs-flx"
   "emacs-guix"
   "emacs-ivy"
   "emacs-lispy"
   "emacs-lsp-mode"
   "emacs-magit"
   "emacs-markdown-mode"
   "emacs-nix-mode"
   "emacs-org"
   "emacs-pass"
   "emacs-plantuml-mode"
   "emacs-protobuf-mode"
   "emacs-rainbow-delimiters"
   "emacs-rg"
   "emacs-strace-mode"
   "emacs-yaml-mode"
   "emacs-yasnippet"
   "ffmpeg"                             ;for work (ffplay on yuv images)
   "file"
   "font-dejavu"
   "gdb"
   "git"
   "git-repo"
   "git:send-email"
   "glibc"                              ;export GUIX_LOCPATH
   "glibc-locales"                      ;provide GUIX_LOCPATH
   "global"
   "gnupg"                              ;gpg-connect-agent
   "graphviz"                           ;dot, gvpr
   "guile"
   "htop"
   "hunspell"
   "hunspell-dict-fr"
   "inetutils"                          ;hostname, traceroute, whois
   "iproute2"                           ;ip
   "libxslt"                            ;xsltproc
   "lilypond"
   "make"
   "man-db"                             ;so Guix builds db and `man -k' works
   "man-pages"
   "mpv"
   "mu"
   "ncdu"
   "net-tools"                          ;netstat
   "nmap"
   "nss-certs"
   "openssh"
   "openvpn"
   "password-store"
   "pinentry-tty"                       ;for pinentry-curses
   "plantuml"
   ;; "qgis"
   "rsync"
   "screen"
   "socat"
   "strace"
   "texlive"
   "tree"
   "ublock-origin-icecat"
   "unzip"
   "vim"
   "vlc"
   "wireguard-tools"
   "youtube-dl"
   ))

;; https://github.com/gorhill/uBlock/issues/2986
;; https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_manifests#linux
;; https://github.com/gorhill/uBlock/wiki/Deploying-uBlock-Origin#customizing-the-settings
(define ub-settings
  (computed-file
   "ub-settings"
   #~(begin
       (use-modules (ice-9 textual-ports))
       (with-output-to-file #$output
         (lambda ()
           (let ((str (lambda (f) (call-with-input-file f get-string-all))))
             (format
              #t (str #$(local-file "firefox/ublock/root.json"))
              (str #$(local-file "firefox/ublock/adminSettings.json")))))))))


(define audacious-config
  (computed-file
   "audacious-config"
   #~(with-output-to-file #$output
       (lambda ()
         ((@@ (ice-9 format) format)
          #t "[amidiplug]~@
              fsyn_soundfont_file=~a;~a~%"
          #$(file-append freepats-gm "/share/soundfonts/FreePatsGM.sf2")
          #$(file-append fluid-3 "/share/soundfonts/FluidR3Mono_GM.sf3"))))))

(home-environment
 (packages
  (append my-private-packages (specifications->packages my-packages)))
 (services
  (list
   my-private-ssh-service
   (service
    home-bash-service-type
    (home-bash-configuration (bashrc (list (local-file "bashrc.bash")))))
   (service
    home-files-service-type
    `((".mozilla/managed-storage/uBlock0@raymondhill.net.json" ,ub-settings)
      (".nix-channels" ,(local-file "nix-channels"))
      (".gitconfig" ,(local-file "gitconfig"))
      (".mozilla/icecat/profiles.ini" ,(local-file "firefox/profiles.ini"))
      (".mozilla/firefox/profiles.ini" ,(local-file "firefox/profiles.ini"))
      (".mozilla/icecat/default/user.js" ,(local-file "firefox/user.js"))
      (".mozilla/firefox/default/user.js" ,(local-file "firefox/user.js"))
      (".gnupg/gpg-agent.conf" ,(local-file "gpg-agent.conf"))
      (".gdbinit" ,(local-file "gdbinit"))
      (".screenrc" ,(local-file "screenrc"))))
   (service
    home-xdg-configuration-files-service-type
    `(("nixpkgs/config.nix" ,(local-file "config.nix"))
      ("git/ignore" ,(local-file "global-gitignore"))
      ("audacious/config" ,audacious-config)))
   (service
    home-channels-service-type
    `(,(channel-with-substitutes-available %default-guix-channel
                                           "https://ci.guix.gnu.org")
      ,(channel (name 'my-guix) (url "file:///home/clement/conf/private"))))
   (simple-service
    'my-home-environment-variables-service-type
    home-environment-variables-service-type
    `(("GUILE_LOAD_PATH" . "/home/clement/conf/private:${GUILE_LOAD_PATH}")
      ;; pass is called from Emacs which is called from Gnome
      ("PASSWORD_STORE_DIR" . "/home/clement/conf/pass")
      ;; foreign distros (Gnone's left bar) need this to know where to look
      ;; for Guix and Nix .desktop files.
      ("XDG_DATA_DIRS" .
       ,(string-append
         "/home/clement/.nix-profile/share:"
         "${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}")))))))
