(setq initial-scratch-message ""
      inhibit-startup-message t
      auth-sources (list (concat (getenv "PASSWORD_STORE_DIR") "/.netrc.gpg"))
      backup-inhibited t
      auto-save-default nil
      column-number-mode t
      user-full-name "Clément Lassieur"
      user-mail-address "clement@lassieur.org"
      visible-bell 1                    ;disable sound
      resize-mini-windows t
      disabled-command-function nil
      custom-file (concat user-emacs-directory "my-custom.el")
      vc-follow-symlinks nil
      display-raw-bytes-as-hex t
      gc-cons-threshold 100000000       ;LSP optim
      read-process-output-max (* 1024 1024) ;LSP optim
      shell-file-name "/bin/bash")
(setq-default indent-tabs-mode nil
              fill-column 78)
(defalias 'yes-or-no-p 'y-or-n-p)

(set-face-attribute 'default nil :font "DejaVu Sans Mono-12")
(load custom-file t)
(when (display-graphic-p) (load-theme 'alect-black-alt t))
(toggle-scroll-bar -1)
(tool-bar-mode -1)
(show-paren-mode)
(evil-mode)
(global-evil-surround-mode 1)
(ivy-mode)
(counsel-mode)
(global-auto-revert-mode)
(savehist-mode)
(global-hi-lock-mode)
(xterm-mouse-mode)
(load-library "lilypond-init.el")
(direnv-mode)
(default-text-scale-decrease)

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-c s") 'rg)
(global-set-key (kbd "C-x f") 'find-file-at-point)

(with-eval-after-load 'ispell
  (setq ispell-program-name "hunspell"))

(with-eval-after-load 'shell
  (add-hook 'shell-mode-hook
            (lambda ()
              (set-variable 'dirtrack-list '("^.*[^ ]+:\\(.*\\)]" 1 nil))
              (dirtrack-mode 1))))

(with-eval-after-load 'tramp
  (add-to-list 'tramp-gvfs-methods "smb"))

(with-eval-after-load 'warnings
  (setq warning-minimum-level :error))

(with-eval-after-load 'epg-config
  (shell-command "gpg-connect-agent /bye") ; in case it's not started
  (setq epg-pinentry-mode 'loopback))

(with-eval-after-load 'info
  (setq Info-additional-directory-list
        '("~/.config/guix/current/share/info")))

(with-eval-after-load 'lilypond-mode
  (setq LilyPond-lilypond-command "lilypond -o ~/syncthing/shared/lilypond"))

(with-eval-after-load 'edebug
  (setq edebug-print-level nil
        edebug-print-length nil))

(with-eval-after-load 'simple
  (setq eval-expression-print-level nil
        eval-expression-print-length nil))

(with-eval-after-load 'mouse
  (setq mouse-yank-at-point t))

(with-eval-after-load 'hi-lock
  (add-hook
   'hi-lock-mode-hook
   (lambda ()
     (highlight-regexp "\\[\\[elisp:\\(.*\\)\\]\\]" 'default 1)
     (highlight-regexp "\\[\\[elisp:(\\(?1:[^) ]*\\).*\\]\\]" 'diary 1))))

(with-eval-after-load 'lsp-clangd
  (setq lsp-clients-clangd-args
        `(,(concat "--query-driver="
                   "/opt/qcom/HEXAGON_Tools/**/bin/*,"
                   "/opt/qcom/SnapdragonLLVMARM/**/bin/*,"
                   "/opt/qcom/qrb5165-aarch64-oe-linux/**/**/usr/bin/**/*"))
        lsp-clients-clangd-library-directories (list "/usr" "/opt")))

(with-eval-after-load 'lsp-treemacs
  (lsp-treemacs-sync-mode))

(with-eval-after-load 'xref
  (add-to-list 'xref-prompt-for-identifier 'xref-find-references t))

(with-eval-after-load 'wgrep
  (setq wgrep-auto-save-buffer t))

(with-eval-after-load 'flymake
  (add-hook 'flymake-diagnostics-buffer-mode-hook
            (lambda () (setq truncate-lines nil))))

(with-eval-after-load 'elisp-mode
  (add-hook 'emacs-lisp-mode-hook 'whitespace-mode)
  (add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'emacs-lisp-mode-hook 'lispy-mode))

(with-eval-after-load 'scheme
  (add-hook 'scheme-mode-hook 'whitespace-mode)
  (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode)
  (add-hook 'scheme-mode-hook 'lispy-mode)
  (add-hook 'scheme-mode-hook 'guix-devel-mode)
  (add-hook 'scheme-mode-hook 'yas-minor-mode))

(with-eval-after-load 'cc-vars
  (setf (cdr (assoc 'other c-default-style)) "linux")
  (add-hook 'c-mode-common-hook 'dtrt-indent-mode)
  (add-hook 'c-mode-common-hook (lambda () (setq indent-tabs-mode t)))
  (add-hook 'c-mode-common-hook 'lsp)
  (add-hook 'c-mode-common-hook 'yas-minor-mode))

(with-eval-after-load 'with-editor
  (add-hook 'with-editor-mode-hook 'evil-normal-state)
  (add-hook 'with-editor-mode-hook 'yas-minor-mode))

(with-eval-after-load 'compile
  (setq compilation-scroll-output 'first-error
        compilation-buffer-name-function
        (lambda (name-of-mode)
          (concat "*" (downcase name-of-mode) "-"
                  (expand-file-name default-directory) "*"))))

(with-eval-after-load 'erc
  (setq erc-current-nick-highlight-type 'nick
        erc-track-exclude-server-buffer t))

(with-eval-after-load 'ivy
  (setq ivy-use-virtual-buffers t
        ivy-re-builders-alist '((t . ivy--regex-ignore-order))
        ivy-virtual-abbreviate 'full
        recentf-max-saved-items nil))

(with-eval-after-load 'yasnippet
  (setq yas-snippet-dirs '("~/src/guix/etc/snippets"))
  (yas-reload-all))

(with-eval-after-load 'whitespace
  (setq whitespace-line-column nil
        whitespace-style '(face trailing lines-tail space-before-tab newline
                                indentation empty space-after-tab)))

(with-eval-after-load 'ob-core
  (setq org-babel-default-header-args
        (cons '(:results . "output replace")
              (assq-delete-all :results org-babel-default-header-args))
        org-babel-default-header-args
        (cons '(:mkdirp . "yes")
              (assq-delete-all :mkdirp org-babel-default-header-args))))

(with-eval-after-load 'org
  (add-hook 'org-mode-hook (lambda () (local-set-key [tab] 'org-cycle)))
  (org-babel-do-load-languages
   'org-babel-load-languages '((emacs-lisp . t) (C . t) (shell . t)))
  (setq org-adapt-indentation nil
        org-directory "~/conf/private/org"
        org-agenda-files (list org-directory)
        org-src-preserve-indentation t
        org-confirm-babel-evaluate nil
        org-confirm-elisp-link-function nil))

(with-eval-after-load 'evil
  (setq evil-default-state 'emacs
        evil-insert-state-modes nil
        evil-motion-state-modes nil)
  (setq-default evil-symbol-word-search t)
  (define-key evil-outer-text-objects-map "w" 'evil-a-symbol)
  (define-key evil-inner-text-objects-map "w" 'evil-inner-symbol)
  (defalias 'forward-evil-word 'forward-evil-symbol)
  (mapc (lambda (mode)
          (evil-set-initial-state mode 'normal))
        '(fundamental-mode prog-mode text-mode conf-mode protobuf-mode
                           pass-view-mode diff-mode LilyPond-mode)))

(with-eval-after-load 'find-dired
  (setq find-ls-option '("-print0 | sort -z | xargs -0 -e ls -lhd" . "-lhd")))

(with-eval-after-load 'rg
  (add-to-list
   'rg-custom-type-aliases
   (cons "cc" (mapconcat (lambda (type)
                           (cdr (assoc type (rg-list-builtin-type-aliases))))
                         '("c" "cpp") " ")))
  (setq rg-command-line-flags
        '("--hidden" "-g !*.git" "--no-messages" "-M 78"
          "--max-columns-preview")))

(with-eval-after-load 'dired
  (setq dired-dwim-target t
        dired-listing-switches "-lha")) ;use ‘C-u s’ to hide hidden files

(with-eval-after-load 'geiser-impl
  (setq geiser-active-implementations '(guile)))

(with-eval-after-load 'magit
  (setq magit-bury-buffer-function 'magit-mode-quit-window
        magit-published-branches '()))

(defun my-garbage-collect ()
  (interactive)
  (magit-shell-command-topdir
   "git -c gc.reflogExpire=0 -c gc.reflogExpireUnreachable=0 -c \
gc.rerereresolved=0 -c gc.rerereunresolved=0 -c gc.pruneExpire=now gc"))

(defun my-shell (arg)
  (let ((name (format "*my-shell: %s*" arg)))
    (if-let ((process (get-buffer-process name)))
        (with-current-buffer name
          (delete-region (process-mark process) (point-max)))
      (save-window-excursion (shell name))
      (accept-process-output (get-buffer-process name)))
    (switch-to-buffer-other-window name)
    (goto-char (point-max))
    (insert arg)
    (comint-send-input nil t)))

(defun my-term (&rest args)
  (let ((name (format "*my-term: %s*" (string-join args " → ")))
        (file (make-temp-file "my-term-")))
    (with-temp-file file
      (dolist (arg args)
        (insert (format "%s\n" arg))))
    (when (get-buffer name) (kill-buffer name))
    (require 'term)
    (term-ansi-make-term name "/bin/bash" file)
    (switch-to-buffer-other-window name)
    (goto-char (point-max))
    (delete-file file)))

(defun my-magit (arg)
  (let (buf)
    (save-window-excursion
      (magit-status-setup-buffer arg)
      (setq buf (current-buffer)))
    (switch-to-buffer-other-window buf)))

(load "~/conf/private/dotfiles/private.el")
